﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    var IMCFollowed = angular.module('IMCFollowed', []);

    var supportTouch = $.support.touch,
            scrollEvent = "touchmove scroll",
            touchStartEvent = supportTouch ? "touchstart" : "mousedown",
            touchStopEvent = supportTouch ? "touchend" : "mouseup",
            touchMoveEvent = supportTouch ? "touchmove" : "mousemove";
    $.event.special.swipeupdown = {
        setup: function () {
            var thisObject = this;
            var $this = $(thisObject);
            $this.bind(touchStartEvent, function (event) {
                var data = event.originalEvent.touches ?
                        event.originalEvent.touches[0] :
                        event,
                        start = {
                            time: (new Date).getTime(),
                            coords: [data.pageX, data.pageY],
                            origin: $(event.target)
                        },
                        stop;

                function moveHandler(event) {
                    if (!start) {
                        return;
                    }
                    var data = event.originalEvent.touches ?
                            event.originalEvent.touches[0] :
                            event;
                    stop = {
                        time: (new Date).getTime(),
                        coords: [data.pageX, data.pageY]
                    };

                    // prevent scrolling
                    if (Math.abs(start.coords[1] - stop.coords[1]) > 10) {
                        event.preventDefault();
                    }
                }
                $this
                        .bind(touchMoveEvent, moveHandler)
                        .one(touchStopEvent, function (event) {
                            $this.unbind(touchMoveEvent, moveHandler);
                            if (start && stop) {
                                if (stop.time - start.time < 1000 &&
                                        Math.abs(start.coords[1] - stop.coords[1]) > 30 &&
                                        Math.abs(start.coords[0] - stop.coords[0]) < 75) {
                                    start.origin
                                            .trigger("swipeupdown")
                                            .trigger(start.coords[1] > stop.coords[1] ? "swipeup" : "swipedown");
                                }
                            }
                            start = stop = undefined;
                        });
            });
        }
    };
    $.each({
        swipedown: "swipeupdown",
        swipeup: "swipeupdown"
    }, function (event, sourceEvent) {
        $.event.special[event] = {
            setup: function () {
                $(this).bind(sourceEvent, $.noop);
            }
        };
    });

    IMCFollowed.controller('HomeController', function ($scope, $rootScope) {

        document.addEventListener('deviceready', onDeviceReady.bind(this), false);

        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener('pause', onPause.bind(this), false);
            document.addEventListener('resume', onResume.bind(this), false);

            localStorage.setItem('Profils',
                JSON.stringify([
                    { id: 0, name: "Laura", birthDate: new Date(1991, 9, 29), height: 162 },
                    { id: 1, name: "Lily", birthDate: new Date(2010, 9, 7), height: 104 }
                ]));

            localStorage.setItem('DatasLaura',
                JSON.stringify([
                    { weight: 51.754, valueDate: new Date(2014, 5, 4) },
                    { weight: 51.473, valueDate: new Date(2014, 5, 12) },
                    { weight: 51.237, valueDate: new Date(2014, 5, 15) }
                ]));

            $scope.profils = JSON.parse(localStorage.getItem('Profils'));
            $rootScope.$apply();

            $scope.addProfil = function () {
                $scope.profils.push({
                    name: $scope.newProfil,
                });
                $scope.newProfil = '';
                event.preventDefault();
            }

            $scope.loadProfil = function (profil) {
                $scope.currentProfil = profil;
                $scope.currentData = JSON.parse(localStorage.getItem('Datas' + profil.name));
                generateGraphic(formatData($scope.currentData));
            }

            document.addEventListener('menubutton', onMenuButton.bind(this), false);
            $(document).on("swipedown swipeup", onMenuButton.bind(this));



            function generateGraphic(data) {
                $('#container').highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'Average Monthly Weather Data for Tokyo'
                    },
                    subtitle: {
                        text: 'Source: WorldClimate.com'
                    },
                    xAxis: [{
                        categories: data.date
                    }],
                    yAxis: [{ // Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        title: {
                            text: 'IMC',
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        opposite: true

                    }, { // Secondary yAxis
                        gridLineWidth: 0,
                        title: {
                            text: 'Weight',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} kg',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }

                    }],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        x: 120,
                        verticalAlign: 'top',
                        y: 80,
                        floating: true,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                    },
                    series: [{
                        name: 'Weight',
                        type: 'line',
                        yAxis: 1,
                        data: data.values,
                        tooltip: {
                            valueSuffix: ' kg'
                        }
                    }, {
                        name: 'IMC',
                        type: 'line',
                        yAxis: 0,
                        data: data.imc,
                        tooltip: {
                            valueSuffix: ' '
                        }
                    }]
                });
            }

            function formatData(data) {
                var dataFormat = new Object();
                dataFormat.date = new Array();
                dataFormat.values = new Array();
                dataFormat.imc = new Array();

                for (var key in data) {
                    dataFormat.date.push(data[key].valueDate);
                    dataFormat.values.push(data[key].weight);
                    dataFormat.imc.push(getImc(data[key].weight));
                }
                return dataFormat;
            }

            function getImc(data) {
                return (data / (($scope.currentProfil.height / 100) * ($scope.currentProfil.height / 100)));
            }
        };

        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        };

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        };

        function onMenuButton() {
            $('[data-role="footer"]').slideToggle(500);
        }
    });
})();