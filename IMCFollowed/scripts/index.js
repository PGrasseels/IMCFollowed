﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.
(function () {
    "use strict";

    var IMCFollowed = angular.module('IMCFollowed', []);

    IMCFollowed.controller('HomeController', function ($scope, $rootScope) {

        document.addEventListener('deviceready', onDeviceReady.bind(this), false);

        function onDeviceReady() {
            // Handle the Cordova pause and resume events
            document.addEventListener('pause', onPause.bind(this), false);
            document.addEventListener('resume', onResume.bind(this), false);

            localStorage.setItem('Profils',
                JSON.stringify([
                    { id: 0, name: "Laura", birthDate: new Date(1991, 9, 29), height: 162 },
                    { id: 1, name: "Lily", birthDate: new Date(2010, 9, 7), height: 104 }
                ]));

            localStorage.setItem('DatasLaura',
                JSON.stringify([
                    { weight: 51.754, valueDate: new Date(2014, 5, 4) },
                    { weight: 51.473, valueDate: new Date(2014, 5, 12) },
                    { weight: 51.237, valueDate: new Date(2014, 5, 15) },
                    { weight: 50.757, valueDate: new Date(2014, 5, 16) },
                    { weight: 50.437, valueDate: new Date(2014, 5, 18) },
                    { weight: 50.037, valueDate: new Date(2014, 5, 21) },
                    { weight: 51.754, valueDate: new Date(2014, 5, 28) },
                    { weight: 52.473, valueDate: new Date(2014, 6, 12) },
                    { weight: 52.237, valueDate: new Date(2014, 6, 15) },
                    { weight: 51.757, valueDate: new Date(2014, 6, 16) },
                    { weight: 52.437, valueDate: new Date(2014, 6, 18) },
                    { weight: 50.037, valueDate: new Date(2014, 6, 21) }
                ]));

            $scope.profils = JSON.parse(localStorage.getItem('Profils'));
            $rootScope.$apply();

            $scope.addProfil = function () {
                $scope.profils.push({
                    name: $scope.newProfil,
                });
                $scope.newProfil = '';
                event.preventDefault();
            }

            $scope.loadProfil = function (profil) {
                $scope.avgIMC = 0;
                $scope.avgWeight = 0;
                $scope.maxWeight = 0;
                $scope.maxIMC = 0;
                $scope.minWeight = 999;
                $scope.minIMC = 999;
                $scope.countIMC = 0;
                $scope.countWeight = 0;
                $scope.currentProfil = profil;
                $scope.currentData = JSON.parse(localStorage.getItem('Datas' + profil.name));
                generateGraphic(formatData($scope.currentData));
            }

            function generateGraphic(data) {
                $('#container').highcharts({
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'IMC & Weight'
                    },
                    subtitle: {
                        text: $scope.currentProfil.name
                    },
                    xAxis: [{
                        categories: data.date,
                        labels: {
                            rotation: -45
                        }
                    }],
                    yAxis: [{ // Primary yAxis
                        floor: $scope.minIMC,
                        ceiling: $scope.maxIMC + ($scope.avgIMC / 2),
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        title: {
                            text: 'IMC',
                            style: {
                                color: Highcharts.getOptions().colors[2]
                            }
                        },
                        opposite: true

                    }, { // Secondary yAxis
                        floor: $scope.minWeight,
                        ceiling: $scope.maxWeight + ($scope.avgWeight / 2),
                        gridLineWidth: 0,
                        title: {
                            text: 'Weight',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} kg',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        }

                    }],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        x: 20,
                        verticalAlign: 'top',
                        y: 20,
                        floating: true,
                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
                    },
                    series: [{
                        name: 'Weight',
                        type: 'line',
                        yAxis: 1,
                        data: data.values,
                        tooltip: {
                            valueSuffix: ' kg'
                        }
                    }, {
                        name: 'IMC',
                        type: 'spline',
                        yAxis: 0,
                        data: data.imc,
                        tooltip: {
                            valueSuffix: ' '
                        }
                    }]
                });
            }

            function formatData(data) {
                var dataFormat = new Object();
                dataFormat.date = new Array();
                dataFormat.values = new Array();
                dataFormat.imc = new Array();

                for (var key = 0; key < data.length; key++) {
                    var weight = data[key].weight;
                    var imc = $scope.getImc(weight);

                    dataFormat.date.push(dateFormat(new Date(data[key].valueDate), "dd/m/yyyy"));
                    dataFormat.values.push(weight);
                    dataFormat.imc.push(imc);

                    if ($scope.minWeight > weight)
                        $scope.minWeight = weight;

                    if ($scope.maxWeight < weight)
                        $scope.maxWeight = weight;

                    if ($scope.minIMC > imc)
                        $scope.minIMC = imc;

                    if ($scope.maxIMC < imc)
                        $scope.maxIMC = imc;

                    $scope.countIMC += imc;
                    $scope.countWeight += weight;
                }

                $scope.avgWeight = ($scope.countWeight / data.length).toFixed(2);
                $scope.avgIMC = ($scope.countIMC / data.length).toFixed(2);

                if ($scope.avgIMC > 40)
                    $scope.currentState = "Morbid obesity or massive";
                else {
                    if($scope.avgIMC > 30 && $scope.avgIMC < 35)
                        $scope.currentState = "Moderate obesity";
                    else {
                        if ($scope.avgIMC > 25 && $scope.avgIMC < 30)
                            $scope.currentState = "Overweight";
                        else {
                            if ($scope.avgIMC > 18.5 && $scope.avgIMC < 25)
                                $scope.currentState = "Normal";
                            else {
                                if ($scope.avgIMC > 16.5 && $scope.avgIMC < 18.5)
                                    $scope.currentState = "Thinness";
                                else {
                                    if ($scope.avgIMC < 16.5)
                                        $scope.currentState = "Undernutrition";
                                }
                            }
                        }
                    }
                }

                return dataFormat;
            }

            $scope.getImc = function(data) {
                return parseFloat((data / (($scope.currentProfil.height / 100) * ($scope.currentProfil.height / 100))).toFixed(2));
            }

            $scope.getDateFormat = function (data) {
                return dateFormat(new Date(data), "dd/m/yyyy");
            }
        };

        function onPause() {
            // TODO: This application has been suspended. Save application state here.
        };

        function onResume() {
            // TODO: This application has been reactivated. Restore application state here.
        };
    });
})();